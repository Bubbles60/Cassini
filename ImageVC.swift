//
//  ImageVC.swift
//  Cassini
//
//  Created by DFINLAY-AIR on 04/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit

class ImageVC: UIViewController {
    // Model use to get data (Image)
    var imageURL: NSURL? {
        didSet{
            image = nil
            // We do not want to fetch if the window is not open (costs money on cell phone)
            // View is top level view / .window is the window it's in
            // so it will be nil if we are not on screen (But what happens when I come on screen see wiew will appear)
            if view.window != nil
            {
            fetchImage()
            }
        
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if image == nil {
            fetchImage()
        }
    }
    
    private func fetchImage()
    {
        if let url = imageURL {
            // Notice this is anetwork call we dont want to execute 
            // on main thread ???
            let imageData = NSData(contentsOfURL: url)
            if imageData != nil
            {
                // Don't forget this will call set
                image = UIImage(data: imageData!)
                
            } else
            {
                image = nil
            }
        }
    }
    
    private var imageView = UIImageView()
    
    private var image: UIImage? {
        get{ return imageView.image}
        set{
            imageView.image = newValue
            // Use like didSet
            imageView.sizeToFit()
        }
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(imageView)
        if image == nil {
            imageURL = DemoURL.Stanford
        }
    }
    //
    
    

}
